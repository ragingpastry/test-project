## [1.0.1](https://gitlab.com/ragingpastry/test-project/compare/v1.0.0...v1.0.1) (2019-09-17)


### Bug Fixes

* fix the thing ([94f6e18](https://gitlab.com/ragingpastry/test-project/commit/94f6e18))

# 1.0.0 (2019-09-17)


### Bug Fixes

* apply fix to the thing ([d9d97fc](https://gitlab.com/ragingpastry/test-project/commit/d9d97fc))
* fix ([416848c](https://gitlab.com/ragingpastry/test-project/commit/416848c))
* tabs? ([1f8aca2](https://gitlab.com/ragingpastry/test-project/commit/1f8aca2))
* why ([4d06dad](https://gitlab.com/ragingpastry/test-project/commit/4d06dad))


### Features

* add releasing ([f5cc5b8](https://gitlab.com/ragingpastry/test-project/commit/f5cc5b8))
* initial release ([984a251](https://gitlab.com/ragingpastry/test-project/commit/984a251))
