FROM python:3.7-alpine as builder
WORKDIR /build
RUN pip install virtualenv
RUN virtualenv /build
RUN apk add --update --no-cache \
    build-base \
    linux-headers \
    pcre-dev \
    git
